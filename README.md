# Customer Alliance Frontend Developer

- The task is to create a Vue app, using the provided screens as reference.
  - The task was create using vue-cli
  - The default configuration can be output through `vue inspect > output.js`
  
- The app should run on the webpack.
  - vue-cli since version 4 runs on webpack.

- I've decided using the store wouldn't be necessary for the application,
  therefore all the state is stored in the CAForm component.

- No external libraries or frameworks were used
- As used Vue-CLI to preconfigure the requirementes, usins sass-dar as the 
  preprocessor for scss.

- Business logic:
  - [x] Given there are 5 types of questions: rating, age, email, password and comment;
  - [x] Once any question is answered, on the right appears a "result widget" with all the given questions and answers.
- On the mobile design:
  - [x] "result widget" is not shown in the mobile view;
  - [x] The age question is split into 3 lines. See "design/3.png".
- [x] Validation rules for the form:
  - Validation for email and password done with regex and computed properties
  - Validation for the other fields takes the value from the corresponding json object
  - [x] The field `"required"` from the example json data must be taken into account;
  - [x] Password must contain at least 1 lowercase letter and a number;
  - [x] Email question must pass a simple validation.
- [x] On Submit – show either "success" or "not valid" somewhere on the page (on your choice).
- [x] Write a good README with basic information (e.g.: how to start the project and how to build it);
- [x] Don't use Bootstrap;
- [x] Preferably use a CSS methodology (such as BEM, etc.)
- [x] Use a CSS preprocessor;
- [x] Commit often and use descriptive commit messages;
- [x] Each question can have a sub-question which appears based on a value given to its parent question (see "design/2.png");
   for example: sub1 appears if parent question got value 1-2
   sub2 appears when parent value 4-5 and nothing appears on value 3
- [ ] Automated tests.
  - I run out of time to write more tests, I've included one simple
  proof of concept test to check if the form component monuts correctly.

## Project setup

```bash
yarn install
```

### Compiles and hot-reloads for development

```bash
yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

### Run your unit tests

```bash
yarn test:unit
```