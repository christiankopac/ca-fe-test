import { shallowMount } from '@vue/test-utils';
import CAForm from '@/components/CAForm.vue';

describe('CAForm.vue', () => {
  it('renders title on form', () => {
    const wrapper = shallowMount(CAForm);
    console.log(wrapper);
    expect(wrapper.contains('h1')).toBe(true);
  });
});
